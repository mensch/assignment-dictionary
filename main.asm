%define CAPACITY 256
%include 'colon.inc'
%include 'lib.inc'
%include 'words.inc'
global _start

section .rodata
prompt: db 'Enter the key: ', 0
read_error: db 'Read Error', 10, 0
found: db 'The key is mapped to: ', 0
not_found: db 'No mapping for the key', 10, 0

section .text

extern find_word
_start:
	mov rdi, prompt
    call print_string
	sub rsp, CAPACITY
    mov rsi, CAPACITY
    mov rdi, rsp
    call read_word
    cmp rax, 0
    jz .err_read
	push rdx ; Сохранение длины искомого слова
    mov rsi, key ; Загрузка начала списка
    mov rdi, rax ; Загрузка указателя на искомое слово
    call find_word
    cmp rax, 0 ; В rax находится адрес начала вхождения в найденный словарь
    je .not_found

.found:
   	add rax, 8 ; Отступ от адреса на следующий словарь
	pop r8
	add rax, r8 ; Отступ от ключа
	inc rax ; Отступ от нуль терминатора
    push rax
	mov rdi, found
	call print_string
	pop rdi
	call print_string
	call print_newline
	mov rdi, 0
.end:
	add rsp, CAPACITY
	call exit

.err_read:
	mov rdi, read_error
	call print_error
	mov rdi, 1
	jmp .end

.not_found:
	mov rdi, not_found
	call print_string
	mov rdi, 0
	jmp .end



print_error:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, 2
    mov rax, 1
    syscall
    ret
