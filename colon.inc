%define key 0

;%1-dictionary key
;%2-label
%macro colon 2
	%2:
		dq key       
		%define key %2
		db %1, 0                                 
%endmacro
