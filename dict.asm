global find_word

section .text

extern string_equals
find_word:
	.loop:
		push rdi            
		push rsi            
		add rsi, 8          
		call string_equals  
		pop rsi             
		pop rdi     
		cmp rax, 0
		jnz .found
		mov rsi, [rsi]     
		cmp rsi, 0
		jnz .loop
	.not_found:
		xor rax, rax        
		ret   
	.found:
		mov rax, rsi        
		ret
