.PHONY: clear

runApp: lib.o dict.o main.o
	ld -o runApp main.o lib.o dict.o
	
%.o: %.asm
	nasm -felf64 -o $@ $<

clear:
	rm *.o
